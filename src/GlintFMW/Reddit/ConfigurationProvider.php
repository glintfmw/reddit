<?php declare(strict_types=1);
	/**
	 * @author Almamu
	 */
	namespace GlintFMW\Reddit;

	use GlintFMW\Configuration\Providers\Provider;

	use GlintFMW\Configuration\Exception\IntegrityException;

	/**
	 * Telegram configuration provider for bots
	 *
	 * @author Almamu
	 */
	class ConfigurationProvider extends Provider
	{
		/**
		 * @inheritdoc
		 */
		function provides() : string
		{
			return "reddit";
		}

		/**
		 * @inheritdoc
		 */
		function convert($input)
		{
		    $keys = array ('username', 'password', 'app_id', 'app_secret', 'user_agent', 'oauth_endpoint', 'basic_endpoint');

		    foreach ($keys as $key)
            {
                if (array_key_exists ($key, $input) == false)
                {
                    throw new IntegrityException ("Reddit configuration needs '" . $key . "' parameter");
                }
            }

			return $input;
		}

		public function getUsername(): string
        {
            return $this->get ('username');
        }

        public function getPassword(): string
        {
            return $this->get ('password');
        }

        public function getAppID(): string
        {
            return $this->get ('app_id');
        }

        public function getAppSecret(): string
        {
            return $this->get ('app_secret');
        }

        public function getUserAgent(): string
        {
            return $this->get ('user_agent');
        }

        public function getOAuthEndpoint(): string
        {
            return $this->get ('oauth_endpoint');
        }

        public function getBasicEndpoint(): string
        {
            return $this->get ('basic_endpoint');
        }
	};